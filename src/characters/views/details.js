import React, { PureComponent } from 'react';
import { fetchCharacterById, clearFetchedCharacter } from 'characters/actions';
import {  withStyles } from '@material-ui/core/styles';
import { Container, Grid, Card, CardMedia, CardHeader, CardContent, Typography, IconButton } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isEmpty } from 'lodash';
import { Load } from 'common/components';
import { SeriesListView } from 'series/views';
import Error from 'common/components/erros';
import EditForm from './edit';
import EditIcon from '@material-ui/icons/Edit';

const styles = theme => ({
    root: {
      padding: theme.spacing(2)
    },
    content: {
      marginTop: theme.spacing(2)
    },
    card: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    cardMedia: {
      height: '100%'
    },
    cardContent: {
      flexGrow: 1,
    },
    sectionTitle: {
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(3)
    }
  });

class CharactersDetails extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            edit: false
        };
    }
    UNSAFE_componentWillMount() {
        const { characterid } = this.props.match.params;
        this.props.fetchCharacterById(characterid);
    }

    componentWillUnmount() {  
        this.props.clearFetchedCharacter();
    }

    handleEditAction() {
        const { edit } = this.state;
        this.setState({
            edit: !edit
        });
    }

    render() {
        const {isLoading, error, fetchedCharacter, classes } = this.props;

        if(!isEmpty(error)) {
            return <Error error={error}/>
        }

        if(isLoading || isEmpty(fetchedCharacter)) {
           return <Load/>
        }
    
        return (
            <div className={classes.root}>
                <Container maxWidth="md">
                    <Grid container spacing={3}>
                        <Grid item xs={12} sm={6} md={3}>
                            <Card className={classes.card}>
                                <CardMedia
                                    component="img"
                                    className={classes.cardMedia}
                                    image={`${fetchedCharacter.thumbnail.path}/portrait_uncanny.jpg`}
                                    title={`Image ${fetchedCharacter.name}`}
                                    />
                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={6} md={9}>
                            { this.state.edit ? <EditForm handleEditAction={this.handleEditAction.bind(this)} character={fetchedCharacter}/> :
                            (<Card className={classes.card}>
                                <CardHeader
                                    title={fetchedCharacter.name}
                                    subheader={`#${fetchedCharacter.id}`}
                                    action={
                                        <IconButton aria-label="settings" onClick={() => this.handleEditAction() }>
                                          <EditIcon />
                                        </IconButton>
                                    }
                                />
                                <CardContent>
                                    <Typography variant="body1" color="textSecondary" component="p" paragraph>
                                    { !isEmpty(fetchedCharacter.description) ? fetchedCharacter.description : "There is no description"}
                                    </Typography>
                                </CardContent>
                            </Card>)
                            }
                        </Grid>
                    </Grid>
                    <Typography className={classes.sectionTitle} variant="h2" component="p">{"Series"}</Typography>
                    <SeriesListView/>
                </Container>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    fetchedCharacter: state.characters.fetchedCharacter,
    isLoading: state.characters.isLoading,
    error: state.error.error
})
  
const mapDispatchToProps = dispatch =>
    bindActionCreators({ fetchCharacterById, clearFetchedCharacter }, dispatch);

export default withStyles(styles)(withRouter(connect(mapStateToProps, mapDispatchToProps)(CharactersDetails)));
