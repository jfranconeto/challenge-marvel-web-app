import React, { PureComponent } from 'react';
import {  withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { fetchCharacters, fetchMoreCharacters } from 'characters/actions';
import { bindActionCreators } from 'redux';
import { Load } from 'common/components';
import { Grid, Card, CardMedia, CardContent, Typography, CardActionArea, TextField } from '@material-ui/core';
import { isEmpty } from 'lodash';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators'

const styles = theme => ({
  root: {
    padding: theme.spacing(3)
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    height: '100%',
  },
  cardContent: {
    flexGrow: 1,
  },
  cardGrid: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(8),
  }
});

export class CharactersList extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      currentCount: 20,
    };

    this.onSearch$ = new Subject();
  }

  UNSAFE_componentWillMount() {
    this.props.fetchCharacters();
  }

  componentDidMount() {    
    this.subscription = this.onSearch$
    .pipe(debounceTime(1000))
    .subscribe(name => this.props.fetchCharacters(name));
  }

  componentWillUnmount() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  showCharacterDetail = id => {
    this.props.history.push(`/characters/${id}`);
  }

  onSearch = e => {
    this.onSearch$.next(e.target.value);
  }

  handleScroll = event => {
    const { fetchMoreCharacters } = this.props;
    const { currentCount } = this.state;
    const element = event.target;
    if (element.scrollHeight - element.scrollTop === element.clientHeight) {
      fetchMoreCharacters({limit: currentCount});
      this.setState({
        currentCount: currentCount + 20,
      });
    }
  };

  render() {
    const {isLoading, characters, classes } = this.props;

    return (
      <Grid container spacing={2}>
            <Grid item xs={12} sm={2}>
              <TextField id="outlined-search" name="name" label="Search by name" type="search" variant="outlined" onChange={this.onSearch}/>
            </Grid>
            <Grid item xs={12} sm={10}>
              {
                isLoading && isEmpty(characters) ? <Load/>:
                (<Grid container spacing={2} onScroll={this.handleScroll} style={ {height: '90vh', overflowY: 'auto' } }>
                {characters.map(character => (
                        <Grid item key={character.id} xs={12} sm={6} md={3}>
                            <Card className={classes.card}>
                              <CardActionArea onClick={() => this.showCharacterDetail(character.id)}>
                                <CardMedia
                                    component="img"
                                    className={classes.cardMedia}
                                    image={`${character.thumbnail.path}/portrait_uncanny.${character.thumbnail.extension}`}
                                    title={`Image ${character.name}`}
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography variant="h5" component="h3">
                                        {character.name}
                                    </Typography>
                                </CardContent>
                              </CardActionArea>
                            </Card>
                        </Grid>  
                      ))
                      }
                  {isLoading && (
                    <div className="text-center">
                      <div
                        className="spinner-border"
                        style={{ width: "4rem", height: "4rem" }}
                        role="status"
                      >
                        <span className="sr-only">Loading...</span>
                      </div>
                  </div>
                  )}
              </Grid> )
              }
              
            </Grid>
         </Grid>
    )
  }
}

CharactersList.propTypes = {
  fetchCharacters: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  characters: state.characters.list,
  isLoading: state.characters.isLoading
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({ fetchCharacters, fetchMoreCharacters }, dispatch);


export default withStyles(styles)(withRouter(connect(mapStateToProps, mapDispatchToProps)(CharactersList)));

