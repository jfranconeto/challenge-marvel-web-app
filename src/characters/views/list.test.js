import React from 'react';
import { render } from '@testing-library/react';
import List from './list';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
 
describe('List React-Redux Component', () => {
  let store;
  let component;
  let props;

  
  beforeEach(() => {
    store = mockStore({
      characters: {
        list: [{
            id: 1011334,
            name: "3-D Man",
            thumbnail: {
                path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
                extension: "jpg"
            }
        }]
      }
    });

    store.dispatch = jest.fn();

    props = {
        fetchCharacters: jest.fn()
    }

    component = render(
    <MemoryRouter>
        <Provider store={store}>
            <List props/>
        </Provider>
    </MemoryRouter>)
  });
 
  it('should render with character in list', () => {
    const { getByText } = component;
    expect(store.dispatch).toHaveBeenCalledTimes(1);
    expect(getByText(/3-D Man/i)).toBeInTheDocument();
  });

});