import { Container, Grid, TextField, Button } from "@material-ui/core"
import React from 'react';
import { useDispatch } from "react-redux";
import { updateCharacter } from 'characters/actions/index';

export default function EditForm({ character, handleEditAction }) {

    const dispatch = useDispatch();

    const handleSubmit = (e) => {   
        const data = {
            name: e.target.name.value,
            description: e.target.description.value
        } 
        dispatch(updateCharacter(data));
        handleEditAction();
        e.preventDefault();
    }

    return (
        <Container component="main">
            <form noValidate autoComplete="off" onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            name="name"
                            variant="outlined"
                            required
                            fullWidth
                            id="name"
                            label="Name"
                            defaultValue={character.name}
                            autoFocus
                            placeholder="Name"
                        />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                        <TextField
                            id="outlined-textarea"
                            name="description"
                            defaultValue={ character.description }
                            label="Description"
                            placeholder='Description'
                            multiline
                            fullWidth
                            rows={9}
                            rowsMax={9}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item sm={2}>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={()=>handleEditAction()}
                        >
                            Cancel
                        </Button>
                    </Grid>
                    <Grid item sm={2}>
                        <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                        >
                            Save
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </Container>
    )
}