import React from 'react';
import { render } from '@testing-library/react';
import Details from './details';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
 
describe('Details React-Redux Component', () => {
  let store;
  let component;
  let props;

  
  beforeEach(() => {
    store = mockStore({
        error: {
            error: null
        },
        characters: {
            fetchedCharacter: {
                id: 1011334,
                name: "A.I.M",
                description: "Teste description",
                thumbnail: {
                    path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
                    extension: "jpg"
                }
            }
        },
        series: {
            list: [{
                id: 2818182,
                title: "Avanger",
                thumbnail: {
                    path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9785",
                    extension: "jpg"
                }
            }]
        }
    });

    store.dispatch = jest.fn();

    props = {
        fetchedCharacter: jest.fn(),
        isLoading: false,
        error: false
    }

    component = render(
    <MemoryRouter>
        <Provider store={store}>
            <Details props/>
        </Provider>
    </MemoryRouter>)
  });
 
  it('should render with character details', () => {
    const { getByText } = component;
    expect(store.dispatch).toHaveBeenCalledTimes(2);
    expect(getByText(/A.I.M/i)).toBeInTheDocument();
    expect(getByText(/Teste description/i)).toBeInTheDocument();
  });

  it('should render with character`s series details', () => {
    const { getByTitle } = component;
    expect(getByTitle(/Image Avanger/i)).toBeInTheDocument();
  });

});