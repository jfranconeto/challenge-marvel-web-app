import * as actions from './index';
import * as types from './types';
import axios from 'axios';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { head } from 'lodash';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
jest.mock('axios');

describe('Characters actions', () => {
    let store;

    beforeEach(() => {
        store = mockStore({});
    });

  it('should create actions to fetch characters', async () => {

    const data = {
        data: {
            data: { 
                results: [
                    {
                        id: 1011334,
                        name: "3-D Man",
                        thumbnail: {
                            path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
                            extension: "jpg"
                        }
                    }
                ] 
             }
        }
    }

    axios.mockImplementationOnce(() => Promise.resolve(data));

    const expectedAction = [
        { type: types.FETCH_CHARACTERS },
        { type: types.FETCHED_CHARACTERS, payload: [...data.data.data.results] }
    ]

    return store.dispatch(actions.fetchCharacters()).then(() => {
        expect(store.getActions()).toEqual(expectedAction);
    });
  })

  it('should create actions to fetch more characters', async () => {

    const data = {
        data: {
            data: { 
                results: [
                    {
                        id: 1011334,
                        name: "A.M.I.",
                        thumbnail: {
                            path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
                            extension: "jpg"
                        }
                    }
                ] 
             }
        }
    }

    axios.mockImplementationOnce(() => Promise.resolve(data));

    const expectedAction = [
        { type: types.FETCH_MORE_CHARACTERS },
        { type: types.FETCHED_MORE_CHARACTERS, payload: [...data.data.data.results] }
    ]

    return store.dispatch(actions.fetchMoreCharacters({limit: 20})).then(() => {
        expect(store.getActions()).toEqual(expectedAction);
    });
  })

  it('should create actions to fetch character by id', async () => {

    const data = {
        data: {
            data: { 
                results: [
                    {
                        id: 1011334,
                        name: "A.M.I.",
                        thumbnail: {
                            path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
                            extension: "jpg"
                        }
                    }
                ] 
             }
        }
    }

    axios.mockImplementationOnce(() => Promise.resolve(data));

    const expectedAction = [
        { type: types.FETCH_CHARACTER_BY_ID },
        { type: types.FETCHED_CHARACTER_BY_ID, payload: head(data.data.data.results) }
    ]

    return store.dispatch(actions.fetchCharacterById(1011334)).then(() => {
        expect(store.getActions()).toEqual(expectedAction);
    });
  })

  it('should create action to clear fetched character', async () => {

    const expectedAction = { type: types.CLEAR_FETCHED_CHARACTER }

    expect(actions.clearFetchedCharacter()).toEqual(expectedAction);
  })

  it('should create action to update character', async () => {

    const update = {
        name: "3-D M",
        description: "Description"
    }
    const expectedAction = { type: types.UPDATE_CHARACTER, payload: {...update} }

    expect(actions.updateCharacter(update)).toEqual(expectedAction);
  })
})