import {
    FETCH_CHARACTERS,
    FETCH_MORE_CHARACTERS,
    FETCHED_CHARACTERS,
    FETCHED_MORE_CHARACTERS,
    FETCH_CHARACTER_BY_ID,
    FETCHED_CHARACTER_BY_ID,
    UPDATE_CHARACTER,
    CLEAR_FETCHED_CHARACTER
} from './types';
import request from 'common/utils/request.js';
import { head } from 'lodash';


export const fetchCharacters = (name) => {
  const fetchAction = {
    type: FETCH_CHARACTERS
  };

  const fetchedAction = {
    type: FETCHED_CHARACTERS
  }

  return async (dispatch) => {
    
    dispatch({ ...fetchAction });

    const { data } = await request({ method: 'GET', url: `/characters${name ? `?name=${name}`: ''}` });
    dispatch({ ...fetchedAction, payload: data.results });
  };
};

export const fetchMoreCharacters = ({name, limit}) => {
  const fetchAction = {
    type: FETCH_MORE_CHARACTERS
  };

  const fetchedAction = {
    type: FETCHED_MORE_CHARACTERS,
    payload: []
  }

  return async (dispatch) => {
    
    dispatch({ ...fetchAction });

    const { data } = await request({ method: 'GET', url: `/characters${name ? `?name=${name}&offset=${limit}`: `?offset=${limit}`}` });
    dispatch({ ...fetchedAction, payload: data.results });
  };
}

export const fetchCharacterById = (id) => {
    const fetchAction = {
      type: FETCH_CHARACTER_BY_ID
    };

    const fetchedAction = {
      type: FETCHED_CHARACTER_BY_ID
    };

    return async (dispatch) => {
      dispatch({ ...fetchAction });
    
      try {
        const { data } = await request({ method: 'GET', url: `/characters/${id}`});
        dispatch({ ...fetchedAction, payload: head(data.results) });
      } catch (error) {
        dispatch({type: "REQUEST_FAIL", error });
      }
    };
};

export const clearFetchedCharacter = () => {
    const action = {
        type: CLEAR_FETCHED_CHARACTER
    };
    return action;
};


export const updateCharacter = (data) => {
  return {
    type: UPDATE_CHARACTER,
    payload: data
  }
}
