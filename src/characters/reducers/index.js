/*
 *
 * Characters reducer
 *
 */

import {
    FETCH_CHARACTERS,
    FETCH_MORE_CHARACTERS,
    FETCHED_CHARACTERS,
    FETCHED_MORE_CHARACTERS,
    FETCH_CHARACTER_BY_ID,
    FETCHED_CHARACTER_BY_ID,
    CLEAR_FETCHED_CHARACTER,
    UPDATE_CHARACTER
  } from 'characters/actions/types';
  
  const initialState = {
    list: [],
    isLoading: false
  };
  
  export const charactersReducer = (state = initialState, action) => {
    switch (action.type) {
      case FETCH_CHARACTERS:
        return { ...state, isLoading: true }
      case FETCHED_CHARACTERS:
        return { ...state, list: action.payload, isLoading: false }
      case FETCH_MORE_CHARACTERS: 
        return { ...state, isLoading: true }
      case FETCHED_MORE_CHARACTERS:
        const { list } = state; 
        return { ...state, list: [...list, ...action.payload] ,isLoading: false }
      case FETCH_CHARACTER_BY_ID:
        return { ...state, isLoading: true }     
      case FETCHED_CHARACTER_BY_ID:
        return { ...state, fetchedCharacter: action.payload, isLoading: false};
      case CLEAR_FETCHED_CHARACTER:
        return { ...state  };
      case UPDATE_CHARACTER:
        const { fetchedCharacter } = state; 
        return { ...state, fetchedCharacter: {...fetchedCharacter, ...action.payload}};
      default:
        return state;
    }
  };
  