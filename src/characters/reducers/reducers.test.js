import { charactersReducer as reducer} from './index';
import * as types from 'characters/actions/types';

describe('Characters reducers', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
        list: [],
        isLoading: false
    })
  })

  it('should handle FETCH_CHARACTERS', () => {
    expect(
      reducer([], {
        type: types.FETCH_CHARACTERS
      })
    ).toEqual({
        isLoading: true
    })
  })

  it('should handle FETCHED_CHARACTERS', () => {
    expect(
      reducer([], {
        type: types.FETCHED_CHARACTERS,
        payload: [{id: 112233, name: "A.M.I"}]
      })
    ).toEqual({   
        list: [{id: 112233, name: "A.M.I"}],
        isLoading: false
    })

    expect(
        reducer({   
            list: [{id: 112233, name: "A.M.I"}],
            isLoading: false
        }, {
          type: types.FETCHED_CHARACTERS,
          payload: [{id: 11111, name: "3-D M"}]
        })
      ).toEqual({   
          list: [{id: 11111, name: "3-D M"}],
          isLoading: false
      })
  })

  it('should handle FETCHED_MORE_CHARACTERS', () => {
    expect(
      reducer(undefined, {
        type: types.FETCHED_MORE_CHARACTERS,
        payload: [{id: 112233, name: "A.M.I"}]
      })
    ).toEqual({   
        list: [{id: 112233, name: "A.M.I"}],
        isLoading: false
    })

    expect(
        reducer({
            list: [{id: 112233, name: "A.M.I"}] 
        }, {
          type: types.FETCHED_MORE_CHARACTERS,
          payload: [{id: 11111, name: "3-D M"}]
        })
      ).toEqual({
          isLoading: false,
          list: [{id: 112233, name: "A.M.I"},  {id: 11111, name: "3-D M"}]
      })
  })
  
  it('should handle FETCHED_CHARACTER_BY_ID', () => {
    expect(
      reducer(undefined, {
        type: types.FETCHED_CHARACTER_BY_ID,
        payload: {id: 112233, name: "A.M.I"}
      })
    ).toEqual({ 
        list: [],  
        fetchedCharacter: {id: 112233, name: "A.M.I"},
        isLoading: false
    })
  })
})