import MinimalLayout from 'common/layouts/minimal';
import {
    CharactersListView,
    CharacterDetailsView
  } from 'characters/views';


export default (() => (
    [
        { path: "/characters", component: CharactersListView, layout: MinimalLayout, exact: true },
        { path: "/characters/:characterid", component: CharacterDetailsView, layout: MinimalLayout, exact: true }
    ]
))()