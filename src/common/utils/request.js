import axios from 'axios';
axios.defaults.baseURL = process.env.API_URL || 'https://gateway.marvel.com:443/v1/public';

export default async (config) => {
    try {
        const response = await axios({
            ...config,
            params: {
                ts: process.env.REACT_APP_TS,
                apikey: process.env.REACT_APP_API_KEY,
                hash: process.env.REACT_APP_HASH
            }
        })
        return response.data
    } catch (error) {
        throw error.response.data;
    }
}
