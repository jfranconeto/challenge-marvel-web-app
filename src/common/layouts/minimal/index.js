import React from 'react';
import PropTypes from 'prop-types';
import Topbar from './components/topbar';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles((theme) => ({
  content: {
    padding: theme.spacing(8, 0, 6)
  }
}));

const Minimal = props => {
  const { children } = props;

  const classes = useStyles();

  return (
    <>
      <Topbar />
      <main>
        <div className={classes.content}>
          { children }
        </div>
      </main>
    </>
  );
};

Minimal.propTypes = {
  children: PropTypes.node
};

export default Minimal;
