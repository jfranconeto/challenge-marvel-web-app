import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import  characterRoutes  from 'characters/routes';
import { RouteWithLayout } from 'common/components';

export default () => {
  return (
    <Switch>
      <Redirect
        exact
        from="/"
        to="/characters"
      />
      {characterRoutes.map((router)=> {
        return (<RouteWithLayout
          key={router}
          component={router.component}
          exact={router.exact}
          layout={router.layout}
          path={ router.path } />)
      })}
      <Redirect to="/characters" />
    </Switch>
  );
};
