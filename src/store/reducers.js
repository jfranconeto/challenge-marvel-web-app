/**
 * Create reducers.
 *
 * Combine all reducers in this file and export the combined reducers.
 */
import { combineReducers } from 'redux';

// Shared reducers
import { charactersReducer } from 'characters/reducers';
import { seriesReducer } from 'series/reducers';

const errorReducer = (state = { error: null }, action) => {
  switch (action.type) {
    case "REQUEST_FAIL":
      return { error: action.error };
    default:
      return {...state, error: null};
  }
}

/**
 * Creates the main reducer with the dynamically injected ones
 */
export default function createReducer(injectedReducers) {
  return combineReducers({
    error: errorReducer,
    characters: charactersReducer,
    series: seriesReducer,
    ...injectedReducers
  });
}
