import {
    FETCH_SERIES_BY_CHARACTER,
    FETCHED_SERIES_BY_CHARACTER,
    CLEAR_FETCHED_SERIES
} from './types';
import request from 'common/utils/request.js';

export const fetchSeriesByCharacter = (character) => {
    const fetchAction = {
      type: FETCH_SERIES_BY_CHARACTER
    };

    const fetchedAction = {
      type: FETCHED_SERIES_BY_CHARACTER
    }
  
    return async (dispatch) => {
        dispatch({...fetchAction});
        const { data } = await request({ method: 'GET', url: `/characters/${character}/series`});
        dispatch({ ...fetchedAction, payload: data.results });
    };
};

export const clearFetchedSeries = () => {
    const action = {
      type: CLEAR_FETCHED_SERIES
    };
    
    return action;
};