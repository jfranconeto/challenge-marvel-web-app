import * as actions from './index';
import * as types from './types';
import axios from 'axios';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
jest.mock('axios');

describe('Series actions', () => {
    let store;

    beforeEach(() => {
        store = mockStore({});
    });

  it('should create actions to fetch series', async () => {

    const data = {
        data: {
            data: { 
                results: [
                    {
                        id: 1011334,
                        title: "Avanger",
                        thumbnail: {
                            path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
                            extension: "jpg"
                        }
                    }
                ] 
             }
        }
    }

    axios.mockImplementationOnce(() => Promise.resolve(data));

    const expectedAction = [
        { type: types.FETCH_SERIES_BY_CHARACTER },
        { type: types.FETCHED_SERIES_BY_CHARACTER, payload: [...data.data.data.results] }
    ]

    return store.dispatch(actions.fetchSeriesByCharacter(1111)).then(() => {
        expect(store.getActions()).toEqual(expectedAction);
    });
  })

  it('should create action to clear fetched series', async () => {

    const expectedAction = { type: types.CLEAR_FETCHED_SERIES }

    expect(actions.clearFetchedSeries()).toEqual(expectedAction);
  })
})