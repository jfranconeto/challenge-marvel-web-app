import React, { PureComponent } from 'react';
import {  withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { fetchSeriesByCharacter, clearFetchedSeries } from 'series/actions';
import { bindActionCreators } from 'redux';
import { Load } from 'common/components';
import { Grid, Container, Card, CardMedia } from '@material-ui/core';


const styles = () => ({
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    height: '100%',
  },
  cardContent: {
    flexGrow: 1,
  }
});

class SeriesList extends PureComponent {


  UNSAFE_componentWillMount() {
    const { characterid } = this.props.match.params;
    this.props.fetchSeriesByCharacter(characterid);
  }

  componentWillUnmount() {  
    this.props.clearFetchedSeries();
  }

  render() {
    const { isLoading, series, classes } = this.props;

    if(isLoading) {
        return <Load/>
    }

    return (
        <Container maxWidth="md">
            <Grid container spacing={3}>
                {series.map(serie => (
                    <Grid item key={serie.id} xs={12} sm={6} md={3}>
                        <Card className={classes.card}>
                            <CardMedia
                                component="img"
                                className={classes.cardMedia}
                                image={`${serie.thumbnail.path}/portrait_uncanny.jpg`}
                                title={`Image ${serie.title}`}
                            />
                        </Card>
                    </Grid>  
                ))
                }
            </Grid>    
        </Container>
    );
  }
}


const mapStateToProps = state => ({
  series: state.series.list,
  isLoading: state.series.isLoading
})

const mapDispatchToProps = dispatch =>
  bindActionCreators({ fetchSeriesByCharacter, clearFetchedSeries }, dispatch);


export default withStyles(styles)(withRouter(connect(mapStateToProps, mapDispatchToProps)(SeriesList)));

