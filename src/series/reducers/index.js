/*
 *
 * Series reducer
 *
 */

import {
    FETCH_SERIES_BY_CHARACTER,
    FETCHED_SERIES_BY_CHARACTER,
    CLEAR_FETCHED_SERIES
  } from 'series/actions/types';
  
  const initialState = {
    list: [],
    isLoading: false
  };
  
export const seriesReducer = (state = initialState, action) => {
    switch (action.type) {
      case FETCH_SERIES_BY_CHARACTER:
        return { ...state, isLoading: true };
      case FETCHED_SERIES_BY_CHARACTER:
        return { ...state, list: action.payload, isLoading: false };
      case CLEAR_FETCHED_SERIES:
        return { ...state };
      default:
        return state;
    }
};
  