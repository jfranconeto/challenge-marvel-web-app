import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from './theme';
import Routes from './Routes';

const history = createBrowserHistory();

export default class App extends Component{
  render() {
    return (
      <ThemeProvider theme={theme}>
        <Router history={history}>
          <Routes/>
        </Router>
      </ThemeProvider>
    );
  }
}
